public class hcode {
    public static int computeHashCode(String key) {
        int hash = 0;
        for (int i = 0; i < key.length(); i++) {
            hash = 31 * hash + key.charAt(i);
        }
        return hash;
    }

    public static void main(String[] args) {
        String key = "Flutter";
        int hashCode = computeHashCode(key);
        System.out.println("Hash code for \"" + key + "\" is: " + hashCode);
    }
}
