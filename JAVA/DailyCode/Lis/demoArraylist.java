import java.util.*;
class demoArraylist{

	public static void main(String arg[]){

		List ar = new ArrayList();

		ar.add(10);
		ar.add(20);
		ar.add(30);
		ar.add(40);
		ar.add(50);

		System.out.println(ar);

		System.out.println("add a element in index");
             	ar.add(1,"sai");
		System.out.println(ar);

		System.out.println("Contain element");
		System.out.println(ar.contains(30));
                
		System.out.println("get a element in index");
		System.out.println(ar.get(1));

		System.out.println("List is empty, return boolean value");
		System.out.println(ar.isEmpty());

		System.out.println("remove element in index");
		System.out.println(ar.remove(2));
		System.out.println(ar);

		System.out.println("set element in index");
		System.out.println(ar.set(0,"Core2web"));
	        System.out.println(ar);

		System.out.println("clear a List");
		ar.clear();
		System.out.println(ar);

	}

}
