class UnaryOperatorPre
{
	public static void main(String []args)
	{
		int x=10;

		System.out.println(+x);
		System.out.println(-x);
		System.out.println(++x);
		System.out.println(--x);
	}
}
/*
 * PreIncrement
 * ++x=
 * int(x)
 * {
 * 	x=x+1;
 * 	return x;
 * }
 **/
