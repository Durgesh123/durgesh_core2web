import java.io.*;
class program1 extends Thread{

	public void run(){

                System.out.println(" Thread - 1 ");
	
	}
}

class program2 extends Thread{

	public void run(){

                System.out.println(" Thread - 2");
	
	}
}

class client{

	public static void main(String arg[])throws InterruptedException{
	 
	 program1 p = new program1();
         program2 p1 = new program2();
         p.start();
	 p1.start();
	 p1.join();
	 p.join();

	  
          System.out.println(" In main");

	}


}
/*class program1 extends Thread {
    public void run() {
        System.out.println("Thread - 1");
    }
}

class program2 extends Thread {
    public void run() {
        System.out.println("Thread - 2");
    }
}

class client {
    public static void main(String[] args) throws InterruptedException {
        program1 p = new program1();
        program2 p1 = new program2();

        p.start();
        p1.start();

        // Wait for both threads to complete
        p.join();
        p1.join();

        // Uncomment if you need a delay of 2 seconds
        // Thread.sleep(2000);

        // Correctly formatted print statement
        System.out.println("In main");
    }
}*/

