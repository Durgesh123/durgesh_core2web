class Thread1 extends Thread{

	public void run(){

               System.out.println(" IN  Thread-1 ");
	}

}
class Thread2 extends Thread{

	public void run(){

               System.out.println(" IN  Thread-2 ");
	}

}
class Thread3 extends Thread{

	public void run(){

               System.out.println(" IN  Thread-3 ");
	}

}
class client{

	public static void main(String arg[])throws InterruptedException{

            Thread1 t1 = new Thread1();
            Thread2 t2 = new Thread2();
            Thread3 t3 = new Thread3();

            t1.setPriority(1);
            t2.setPriority(5);
            t3.setPriority(10);
            
	    
	    t1.start();
	    t2.start();
	    t3.start();

	    t1.join();
	    t2.join();
	    t3.join();


	    System.out.println(" In main ");



	}

}
