import java.util.*;
import java.io.*;
class Propertiesdemo{

	public static void main(String arg[])throws IOException{
  
           Properties p = new Properties();
      
           FileInputStream fis = new FileInputStream("Student.properties");
           p.load(fis);
          
	   for(int i = 1; i <=3; i++){
             
           String name =p.getProperty("Student"+i+".name");
           String roll =p.getProperty("Student"+i+".roll");
           String id =p.getProperty("Student"+i+".id");
           
	   System.out.println("Student :"+i);
	   System.out.println("StudentName :" + name);
	   System.out.println("roll :" + roll);
	   System.out.println("id :" + id);

	   System.out.println();
           
           }	   


	}
}
