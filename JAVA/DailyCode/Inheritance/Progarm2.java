class parent{

     static void fun(){

              System.out.println(" parent - fun");
     }

}
class child extends parent{

	static void fun(){

             System.out.println("child - fun ");
	}

}
class client{

	public static void main(String arg[]){

	  parent obj = new parent();
     	  obj.fun();

          child obj2 = new child();
           obj2.fun();
    
          parent obj3 = new parent();
   	   obj3.fun();

	}

}
