class program22{

		public static void main(String arg[]){

			int arr[] = new int[]{2,4,1,3};
			int N = 4;
			int psArr[] = new int[N];
			 psArr[0] = arr[0];

			 for(int i =1; i<N; i++){
				psArr[i] = psArr[i - 1] + arr[i];
			 }

			 for(int i = 0; i<N; i++){
				for(int j = i; j < N; j++){
					int sum = 0;
					if( i == 0)
						sum = psArr[j];
					else
						sum = psArr[j] - psArr[i - 1];

					System.out.println(sum);
				}
			 }
		}

}
