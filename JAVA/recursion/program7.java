class program7{

     int fun(int N){	
	int sum = 0;
	while ( N != 0){

		int digit = N % 10;
		N = N / 10;
		sum = sum + digit;	

	}
	return sum;
     }

    public static void main(String arg[]){
	
        int N = 123;
	 program7 pg = new program7();
	 System.out.println(pg.fun(N));
    } 
 }


