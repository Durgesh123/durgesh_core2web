class program15{

	  String fun(String str){

			if(str == null || str.length() <= 1)
				return str;


			return fun(str.substring(1)) + str.charAt(0);

	  }
	public static void main(String arg[]){

		String str = "core2web";
		program15 pg = new program15();
		String ret = pg.fun(str);
		System.out.println(ret);
	}

}
