class program13{

	int fun(int num){

		if(num == 0)
			return 1;

		return num * fun(num - 1);

	}
	public static void main(String arg[]){

	program13 pg = new program13();
	int ret = pg.fun(5);
	System.out.println(ret);

	}

}
