import java.util.*;
class program2{
        
	int binarySearch(int arr[],int start,int end,int search){
	
		if(start > end){
		
			return -1;
		}else{
                 
		int mid = (start + end) / 2;

		if(arr[mid] == search){
			return mid;
		}	

		else if(arr[mid] < search){
		        return binarySearch(arr,mid + 1,end,search);
		}
	         else{
	       	        return binarySearch(arr,start,mid - 1,search);		 
		  }	

	       }
	  
	}
	public static void main(String arg[]){

	int arr[] = new int[]{1,2,5,7,11,15,17};
	
	program2 pg = new program2();
	Scanner sc = new Scanner(System.in);
	int search = sc.nextInt();
	int start = 0;
	int end = arr.length - 1;
 	int res = pg.binarySearch(arr,start,end,search);

	if(res == -1)
		System.out.println("element not found in index");
	else
		System.out.println("element found in index :"+res);


	}

}
