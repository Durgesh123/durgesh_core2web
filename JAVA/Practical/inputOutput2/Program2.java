import java.io.*;
class Program2{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(BR.readLine());

		for(int i=1;i<=row;i++){
			char ch='a';
			for(int j=1;j<=i;j++){
				if(i%2==0){
					System.out.print("$"+" ");
				}
				else{
					System.out.print(ch++ +" ");
				}
			}
			System.out.println();
		}
	}
}

