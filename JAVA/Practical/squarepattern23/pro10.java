import java.io.*;
class pro10{
   
	public static void main(String arg[])throws IOException{
            
               BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		    int row=Integer.parseInt(br.readLine());
		    int num=row*row;
		   for(int i=1;i<=row;i++){
                        
			   for(int j=1;j<=row;j++){
                            if( i == j){
                          
				     System.out.print("$"+"\t");
			    }else{
				    int mul=num*j;
                                
                                     System.out.print(mul+"\t");
			    }
			    num--;
                              
			   }
			   System.out.println();

		   }

	   }

}
