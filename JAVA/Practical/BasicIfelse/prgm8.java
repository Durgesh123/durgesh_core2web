class Ifelse{
	public static void main(String[] args){
		float num = 85.00f;
		if(num>= 85.00){
			System.out.println("first class with destination");
		}else if(num>=75.00 && num<85.00){
			System.out.println("first class");
		}else if(num>=65.00 && num<75.00){
			System.out.println("Second class");
		}else if(num>=55.00 && num<65.00){
			System.out.println("Pass");
		}else{
			System.out.println("Fail");
		}
	}
}
