import java.io.*;
class pro8{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size=Integer.parseInt(br.readLine());
		char arr[]=new char[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=(char)br.read();
		}
		for(int i=0;i<arr.length/2;i++){
			char temp=arr[i];
			arr[i]=arr[size-1-i];
			arr[size-1-i]=temp;
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
	}
}

