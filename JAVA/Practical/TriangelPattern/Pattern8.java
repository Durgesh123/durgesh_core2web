import java.io.*;
class Pattern8{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows");
		int row=Integer.parseInt(BR.readLine());

		for(int i=1;i<=row;i++){
			int num=i;
			for(int j=1;j<=row-i+1;j++){
				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}


