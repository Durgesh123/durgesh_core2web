import java.io.*;
class Pattern6{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter a row : ");
		int row=Integer.parseInt(BR.readLine());

		for(int i=1;i<=row;i++){
			int num=row-i+1;
			for(int j=1;j<=num;j++){
				System.out.print(num+" ");
			}
			System.out.println();
		}
	}
}


