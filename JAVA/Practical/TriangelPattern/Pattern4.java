import java.io.*;
class Pattern4{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a rows :");
		int row=Integer.parseInt(BR.readLine());
		
		for(int i=1;i<=row;i++){
			char ch=(char)(64+i);
			for(int j=1;j<=i;j++){
				System.out.print(ch+" ");
				ch+=1;
			}
		System.out.println();
	}
}
}
	
