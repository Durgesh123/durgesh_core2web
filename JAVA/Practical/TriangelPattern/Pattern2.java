import java.io.*;
class Pattern2{
	public static void main(String[]args)throws IOException{
		BufferedReader BR=new BufferedReader(new InputStreamReader (System.in));
		System.out.print("Enter a rows : ");
		int row=Integer.parseInt(BR.readLine());
		
		for(int i=1;i<=row;i++){
			int num=1;
			for(int j=1;j<=i;j++){
				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}

