import java.util.*;
class Quicksort{

	int partition(int arr[],int start,int end){
		int pavi = arr[end];
		int i = start - 1;
		for(int j = start; j < arr.length; j++){
			if(arr[j] < pavi){
			i++;
			int temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		     }	

		}

			i++;
			int temp = arr[end];
			arr[end] = arr[i];
			arr[i] = temp;
			return i;

	}
	void quicksort(int arr[],int start, int end){
	
		if(start < end){	
		int pavi = partition(arr,start,end);
		quicksort(arr,start,pavi - 1);
		quicksort(arr,pavi+1,end);
		}	

	}
	public static void main(String arg[]){

	int arr[] = new int[]{12,7,6,14,5,15,10};
	System.out.println(Arrays.toString(arr));

	Quicksort qs = new Quicksort();
	qs.quicksort(arr,0,arr.length-1);

	System.out.println(Arrays.toString(arr));

	}

}
