class memo{

	static void fun(int arr[], int num){
		if(num <= 1)
			return;

		fun(arr,num - 1);

		int i = num - 2;
		int mid = i;
		for(int j = i + 1; j < arr.length; j++){
			if(arr[j] < arr[mid])
				mid = j;
		
		}
		int temp = arr[i];
		arr[i] = arr[mid];
		arr[mid] = temp;


	}

	public static void main(String arg[]){
        
	int arr[] = new int[]{9,2,7,3,1,8,4,6};

       	  fun(arr,arr.length);

	for(int i = 0; i < arr.length;i++){

		System.out.print(arr[i] + " ");
	}		
		System.out.println();
	}
}

