class equilibrium {

    public static void main(String[] args) {

        int arr[] = new int[]{2, 2, 3,1,3};

        // Calculate the total sum of the array
        int totalSum = 0;
        for (int i = 0; i < arr.length; i++) {
            totalSum += arr[i];
        }

        int leftSum = 0;
        boolean foundEquilibrium = false;

        // Iterate through the array to find the equilibrium index
        for (int i = 0; i < arr.length; i++) {
            // Calculate the right sum as totalSum - leftSum - current element
            int rightSum = totalSum - leftSum - arr[i];

            if (leftSum == rightSum) {
                System.out.println("Equilibrium index found at: " + i);
                foundEquilibrium = true;
                break;
            }

            // Update the left sum for the next iteration
            leftSum += arr[i];
        }

        if (!foundEquilibrium) {
            System.out.println("No equilibrium index found.");
        }
    }

}
