class program4{

	public static void main(String arg[]){

	int arr[] = new int[]{-3,4,-2,5,3,-2,8,2,1,4};
	int k = 4;
	int start = 0;
	int end = k - 1;
	int maxsum = Integer.MIN_VALUE;
	int psarr[] = new int[arr.length];
	psarr[0] = arr[0];

	for(int i = 1; i < arr.length; i++){

		psarr[i] = psarr[i - 1] + arr[i];
	}

	while(end < arr.length){

		int sum = 0;

		if(start == 0)
			sum = psarr[end];
		else
			sum = psarr[end] - psarr[start - 1];


		if(sum > maxsum)
			maxsum = sum;
			
		start++;
		end++;
	}

	System.out.println(maxsum);  //  N + (N - K)  => O(N)

	}

}
